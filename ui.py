from tkinter import *
import math
import pyautogui
from halo import PredictingNetwork
from tkinter import ttk

canvas_width = 1000
canvas_height = 700
greenLine = 17

canvas_half_height = canvas_height / 2
canvas_half_width  = canvas_width  / 2
canvas_half_diag2 = canvas_half_width**2 + canvas_half_height**2
canvas_center = (canvas_half_width, canvas_half_height)


def dist2(x1, y1, x2, y2):
    return (x1-x2)**2 + (y1-y2)**2

def getLineYinX(x, line):
    '''Raetiti a pontot a vonalra

    x: x
    
    line: ((start_x, start_y), (end_x, end_y))
    ''' 
    ((s_x, s_y), (e_x, e_y)) = line

    return (e_y - s_y) * (x - s_x) / (e_x - s_x) + s_y
class Suggestion:
    
    def __init__(self):
        self.x = 0
        self.y = 0
        self.text = 'text_01'
        self.prob = 0.5
        self.uiText = 'text01'

    @property
    def fontSize(self):
        maxFont = 50
        minFont = 5

        if self.x < canvas_half_width:
            return maxFont

        def getTextDistance():
            return dist2(canvas_half_width, canvas_half_height, self.x, self.y)

        distance = math.floor(getTextDistance())
        if distance == 0:
            return maxFont

        complDistance = canvas_half_diag2 - distance
        return math.floor((maxFont-minFont)*complDistance/canvas_half_diag2) + minFont
    
    def move(self, dist, projected_Y):
        
        offset_x = 0.01
        v_x, v_y = self.x - (canvas_width + offset_x), self.y - projected_Y
        unit_dist = math.sqrt(dist2(self.x, self.y, canvas_width + offset_x, projected_Y))

        vect_multip = ((unit_dist + dist) / unit_dist)
        self.x, self.y = canvas_width + v_x * vect_multip + offset_x, projected_Y + v_y * vect_multip
        #self.y = self.y + (self.y - canvas_half_height) * 0.002
        #'''
        self.x = self.x - dist * 0.01
        y_dist = (self.y - getLineYinX (self.x, (canvas_center, (canvas_width, projected_Y))))
        self.y = self.y + \
            y_dist * (canvas_width - self.x) * 0.00004\
            +  y_dist * dist * 0.0008

class UI:
    def __init__(self):
        self.master = Tk()

        self.canvas = Canvas(self.master, width=canvas_width, height=canvas_height)
        self.canvas.pack()
        self.canvas.xview_moveto(0)
        self.canvas.yview_moveto(0)

        self.cursor = None
        self.suggestionTextElements = []

        self.container = Frame(self.canvas, borderwidth=1, relief="sunken", width=300, height=300)
        self.container.grid_propagate(False)
        self.container.pack(side="top", fill="both", expand=True)
        self.container.grid_rowconfigure(0, weight=1)
        self.container.grid_columnconfigure(0, weight=1)

        self.textBox = Text(self.container, width=50, font=("Purisa", 11))
        self.textBox.grid(row=0, column=0, sticky="nsew")

        self.textBox.pack()
       
        self.drawBG()

    def repaint(self, written_text):
        self.deleteDynamicElements ()
        self.drawBG()
        self.drawCursor(*self.getCursorPosition())
        #print (self.getCursorPosition ())
        self.drawWrittenText (written_text)

    def deleteDynamicElements (self):
        self.canvas.delete ('all')
        for textElement in self.suggestionTextElements:
            self.canvas.delete (textElement)

        self.suggestionTextElements.clear ()

    def update(self, suggestions, written_text):
        self.repaint(written_text)
        self.drawSuggestions(suggestions)
        self.master.update()

    def drawBG(self):
        kisVonalFelHossz = canvas_height/greenLine
        self.canvas.create_line (canvas_half_width, 0, canvas_half_width, canvas_half_height - kisVonalFelHossz, fill="red", width=8)
        self.canvas.create_line (canvas_half_width, canvas_half_height + kisVonalFelHossz, canvas_half_width, canvas_height, fill="red", width=8)
        self.canvas.create_line (canvas_half_width, canvas_half_height - kisVonalFelHossz, canvas_half_width, canvas_half_height + kisVonalFelHossz, fill="green", width=5)  
        self.canvas.create_window((250, 300), window=self.container)

    def drawCursor(self, cursor_x, cursor_y):
        self.cursor = self.canvas.create_line (canvas_half_width, canvas_half_height, cursor_x, cursor_y, fill="blue", width=3)

    def getCursorPosition(self):
        return (self.master.winfo_pointerx() - self.master.winfo_rootx(), self.master.winfo_pointery() - self.master.winfo_rooty())

    def drawSuggestion (self, suggestion):
        '''
        if (suggestion.text[-1] == ' '):
            suggestion.text =suggestion.text[:-1] + suggestion.text[-1].replace(' ', u"\u2610")

        textElement = self.canvas.create_text(suggestion.x, suggestion.y, 
                                              fill="darkblue", 
                                              font=("Purisa", suggestion.fontSize), 
                                              text=suggestion.text)
        self.suggestionTextElements.append (textElement)

        if (suggestion.text[-1] == u"\u2610"):
            suggestion.text =suggestion.text[:-1] + suggestion.text[-1].replace( u"\u2610", ' ')
        '''
        self.canvas.create_text(suggestion.x, suggestion.y, 
                                              fill="darkblue", 
                                              font=("Purisa", suggestion.fontSize), 
                                              text=suggestion.uiText)
    def drawWrittenText (self, text):
        self.textBox.delete (1.0, 'end')
        self.textBox.insert ('end', text)

    def drawSuggestions(self, suggestions):
        for sug in suggestions:
            self.drawSuggestion(sug)

class SuggestionHandler:
    def __init__(self, ui :UI):
        self.needNewSuggestions = True
        self.suggestions = []
        self.suggestionProbs = []
        self.ui = ui
        self.written_text = "I"
        self.predictor = PredictingNetwork()

    def fillSuggestionProbs(self):
        text = self.written_text[-40:].lower ()
        if len (text) < 40:
            emptyArray = ' ' * 40
            text = (emptyArray + text)[-40:]
        self.suggestionProbs = self.predictor.predict_completions(text, n=10)
        self.suggestionProbs += self.predictor.predict_characters (text)
        print (self.suggestionProbs)

    def updateSuggestions(self):
        if self.needNewSuggestions:
            self.needNewSuggestions = False
            self.suggestions = []
            self.fillSuggestionProbs()
            def sortF(x):
                return x[0]
            self.suggestionProbs.sort(key=sortF)
            numberOfSuggestions = len (self.suggestionProbs)
            for i, (text, probability) in enumerate (self.suggestionProbs):
                suggestion = Suggestion ()
                suggestion.x = canvas_width - probability * canvas_half_width * 0.5 - 10
                suggestion.y = (2 * i + 1) * (canvas_height / (2 * numberOfSuggestions))
                suggestion.text = text
                suggestion.prob = probability
                suggestion.uiText = text.replace(' ', u"\u2610")                
                self.suggestions.append (suggestion)

        cursor_x, cursor_y = ui.getCursorPosition ()
        projected_Y = getProjection (cursor_x, cursor_y)
        move_dist = getMoveDist (cursor_x, cursor_y) * 0.02
        for suggestion in self.suggestions:
            suggestion.move (move_dist, projected_Y)
            if self.chooseSuggestion (suggestion):
                self.needNewSuggestions = True
                self.written_text += suggestion.text
                break
                
        self.needNewSuggestions = self.needNewSuggestions or not self.isAnySuggestionOnCanvas()
    
    def chooseSuggestion (self, suggestion):
        kisVonalFelHossz = canvas_height/greenLine
        
        return suggestion.x < canvas_half_width and \
                suggestion.x > canvas_half_width - 10 and \
                suggestion.y < canvas_half_height + kisVonalFelHossz and \
                suggestion.y > canvas_half_height - kisVonalFelHossz

    def isAnySuggestionOnCanvas(self):
        for suggestion in self.suggestions:
            if suggestion.x < canvas_width and \
                suggestion.x > canvas_half_width and \
                suggestion.y < canvas_height and \
                suggestion.y > 0:
                return True
        return False
    

def getProjection (cursor_x, cursor_y):
    if cursor_x == canvas_half_width:
        return canvas_half_height

    return getLineYinX(canvas_width, ((canvas_half_width, canvas_half_height),(cursor_x, cursor_y)))

def getMoveDist (cursor_x, cursor_y):
    distance = math.sqrt(dist2(cursor_x, cursor_y, canvas_half_width, canvas_half_height))

    if cursor_x < canvas_half_width:
        distance = -distance

    return distance

ui = UI()
sug = SuggestionHandler(ui)

while True:
    sug.updateSuggestions()
    
    ui.update(sug.suggestions, sug.written_text)