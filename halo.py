import numpy as np
import tensorflow as tf
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation
from keras.layers import LSTM
from keras.layers.core import Dense, Activation
import pickle
import sys
import heapq

SEQUENCE_LENGTH = 40

class PredictingNetwork:


    def __init__(self):
        with open('indices_char.pickle', 'rb') as handle:
            self.indices_char = pickle.load(handle)
        with open('char_indices.pickle', 'rb') as handle:
            self.char_indices = pickle.load(handle)
        self.model = load_model('keras_model_acc054_3ep.h5')
        self.history = pickle.load(open("history_acc054_3ep.p", "rb"))
        self.prepare_input("")

    def prepare_input(self, text):
        text = text.lower()
        x = np.zeros((1, SEQUENCE_LENGTH, len(self.char_indices)))
        for t, char in enumerate(text):
            x[0, t, self.char_indices[char]] = 1.
            
        return x

    def sample(self, preds, top_n=3):
        preds = np.asarray(preds).astype('float64')
        preds = np.log(preds)
        exp_preds = np.exp(preds)
        preds = exp_preds / np.sum(exp_preds)
        
        return heapq.nlargest(top_n, range(len(preds)), preds.take)

    def predict_completion(self, text):
        original_text = text
        generated = text
        completion = ''
        while True:
            x = self.prepare_input(text)
            preds = self.model.predict(x, verbose=0)[0]
            next_index = self.sample(preds, top_n=1)[0]
            next_char = self.indices_char[next_index]
            text = text[1:] + next_char
            completion += next_char
            
            if len(original_text + completion) + 2 > len(original_text) and next_char == ' ':
                return completion

    def predict_completions(self, text, n=3):
        x = self.prepare_input(text)
        preds = self.model.predict(x, verbose=0)[0]

        next_indices = self.sample(preds, n)
        
        return [ (self.indices_char[idx] + self.predict_completion(text[1:] + self.indices_char[idx]), preds[idx]) for idx in next_indices]


    def predict_characters(self, text):
        x = self.prepare_input(text)

        preds = self.model.predict(x, verbose=0)[0]
        next_indices = self.sample(preds, len (preds))

        return [ (self.indices_char[idx], pred) for idx, pred in enumerate (preds)]
